using System.Reflection;
using System.Runtime.CompilerServices;


[assembly: AssemblyTitle("Antick.Db")]
[assembly: AssemblyDescription("Components to work with DB")]
[assembly: AssemblyCompany("Antick")]
[assembly: AssemblyProduct("Antick.Db")]
[assembly: AssemblyCopyright("Copyright � Antick 2017")]
[assembly: AssemblyTrademark("Antick.Db")]
[assembly: AssemblyCulture("")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]