﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Antick.Db
{
    public class SessionBuilder<TContext> : ISessionBuilder<TContext> where TContext : DbContext
    {
        private readonly ISessionFactory<TContext> m_SessionFactory;

        public SessionBuilder(ISessionFactory<TContext> sessionFactory)
        {
            m_SessionFactory = sessionFactory;
        }

        public void Execute(Action<ISession<TContext>> action)
        {

            var session = default(ISession<TContext>);
            try
            {
                session = m_SessionFactory.Create();

                action(session);
            }
            finally
            {
                if (session != null)
                    m_SessionFactory.Release(session);
            }
        }

        public TResult Execute<TResult>(Func<ISession<TContext>, TResult> action)
        {

            var session = default(ISession<TContext>);
            try
            {
                session = m_SessionFactory.Create();

                var result = action(session);

                return result;
            }
            finally
            {
                if (session != null)
                    m_SessionFactory.Release(session);
            }
        }

        public async Task ExecuteAsync(Action<ISession<TContext>> action)
        {
            await Task.Factory.StartNew(() =>
            {
                var session = default(ISession<TContext>);
                try
                {
                    session = m_SessionFactory.Create();

                    action(session);
                }
                finally
                {
                    if (session != null)
                        m_SessionFactory.Release(session);
                }
            });
        }

        public async Task<TResult> ExecuteAsync<TResult>(Func<ISession<TContext>, TResult> action)
        {
            return await Task.Factory.StartNew(() =>
            {

                var session = default(ISession<TContext>);
                try
                {
                    session = m_SessionFactory.Create();

                    var result = action(session);

                    return result;
                }
                finally
                {
                    if (session != null)
                        m_SessionFactory.Release(session);
                }
            });
        }
    }
}
