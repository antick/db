﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Antick.Db
{
    /// <summary>
    /// Сессия доступа ко всем сущностям указаного типа Контекста
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISession<T> : IDisposable where T : DbContext
    {
        /// <summary>
        /// Добавление в контекст новой сущности
        /// </summary>
        void Add<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Добавление в контекст массива сущностей (Bulk Insert)
        /// </summary>
        Task<object> BulkInsertAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;

        /// <summary>
        /// Аттач сущности к контексту и установка его статуса на обновление
        /// </summary>
        void Update<TEntity>(TEntity entity) where TEntity : class;
        
        /// <summary>
        /// Удаление сущности из контекста
        /// </summary>
        void Delete<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Доступ к построителю запроса по указаной сущности
        /// </summary>
        IQueryable<TEntity> Query<TEntity>() where TEntity : class;


        /// <summary>
        /// Применение всех изменений в данном контексте
        /// </summary>
        void Save();

        void SaveAsync();
    }
}
