﻿using System.Data.Entity;

namespace Antick.Db
{
    /// <summary>
    /// Фабрика производства DbContext для EntityFrameWork
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDbContextFactory<T> where T: DbContext
    {
        T Create();
        void Release(T dbService);
    }
}
