﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;

namespace Antick.Db
{
    public class Session<T> : ISession<T> where T : DbContext 
    {
        private readonly IDbContextFactory<T> m_DbContextProvider;
        protected readonly T DbContext;

        public Session(IDbContextFactory<T> dbContextProvider)
        {
            m_DbContextProvider = dbContextProvider;
            DbContext = m_DbContextProvider.Create();

            ((IObjectContextAdapter)DbContext).ObjectContext.ObjectMaterialized +=
                (sender, e) => DateTimeUtcFix.Apply(e.Entity);
        }

        public void Add<TEntity>(TEntity entity) where TEntity: class
        {
            DbContext.Set<TEntity>().Add(entity);
        }

        public async Task<object> BulkInsertAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            await  DbContext.BulkInsertAsync(entities);
            return Task.FromResult(new object());
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class
        {
            DbContext.Set<TEntity>().Attach(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            DbContext.Set<TEntity>().Remove(entity);
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class
        {
            return DbContext.Set<TEntity>();
        }

        public void Save()
        {
            DbContext.SaveChanges();
        }

        public void SaveAsync()
        {
            DbContext.SaveChanges();
        }

        public void Dispose()
        {
            m_DbContextProvider.Release(DbContext);
        }
    }
}
