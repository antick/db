﻿using System.Data.Entity;

namespace Antick.Db
{
    public interface ISessionFactory<TContext> where TContext: DbContext
    {
        ISession<TContext> Create();

        void Release(ISession<TContext> entitySession);
    }
}
