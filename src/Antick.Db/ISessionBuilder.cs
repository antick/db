﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Antick.Db
{
    public interface ISessionBuilder<TContext> where TContext : DbContext
    {
        /// <summary>
        /// Исполнить Action на указанной сессии
        /// </summary>
        /// <param name="action"></param>
        void Execute(Action<ISession<TContext>> action);

        /// <summary>
        /// Исполнить Function на указанной сессии и возвратить результат
        /// </summary>
        TResult Execute<TResult>(Func<ISession<TContext>, TResult> action);

        /// <summary>
        /// Исполнить Action на указанной сессии
        /// </summary>
        /// <param name="action"></param>
        Task ExecuteAsync(Action<ISession<TContext>> action);

        /// <summary>
        /// Исполнить Function на указанной сессии и возвратить результат
        /// </summary>
        Task<TResult> ExecuteAsync<TResult>(Func<ISession<TContext>, TResult> action);
    }
}
