﻿using System.Data.Entity;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Inceptum.AppServer.Configuration;

namespace Antick.Db
{
    public class SessionBuilderWindsorInstaller<T> : IWindsorInstaller where T: DbContext
    {
        private readonly string m_ConnectionStringName;

        public SessionBuilderWindsorInstaller(string connectionStringName)
        {
            m_ConnectionStringName = connectionStringName;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<T>().WithConnectionStrings(new { connectionString = m_ConnectionStringName }).LifestyleTransient(),
                Component.For<IDbContextFactory<T>>().AsFactory().LifestyleSingleton(),
                Component.For<ISession<T>>().ImplementedBy<Session<T>>().LifestyleTransient(),
                Component.For<ISessionFactory<T>>().AsFactory().LifestyleSingleton(),
                Component.For<ISessionBuilder<T>>().ImplementedBy<SessionBuilder<T>>().LifestyleSingleton()
            );
        }
    }
}
